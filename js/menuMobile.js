function openMenuMobile(){
    document.querySelector('.header_nav').classList.add('open');
    document.querySelector('.overlay_menu_mobile').classList.add('open');
}

function closeMenuMobile(){
    document.querySelector('.header_nav').classList.remove('open');
    document.querySelector('.overlay_menu_mobile').classList.remove('open');
}

document.getElementById("closeButton").addEventListener("click", function() {
    closeMenuMobile();

    // Masquer le menu burger en changeant son style CSS
    const burgerIcon = document.getElementById("burgerIcon");
    burgerIcon.style.display = "none";
});